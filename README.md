# Taller-React
Taller enfocado en conceptos básicos de ReactJs así como también
un programa de un buscador en la web usando datos desde una API.

Para ejecutarlo:
1. Descargar la versión más reciente de Nodejs: https://nodejs.org/en/
2. Entrar a la carpeta del proyecto
3. Ejecutar el siguiente comando para descargar las dependencias: npm install
4. Ejecutar el siguiente comando para ejecutra la aplicación: npm start

