import SearchBox from './components/SearchBox';
import './style.css';
import {useEffect, useState} from 'react';
import SearchResults from './components/SearchResults';
import axios from "axios";

export default function Search(){

    const [isAtTop, setIsAtTop]=useState(false);
    //Se usaria si viniera la data de una api
    const [data, setUsersData]=useState([]);
    const [results, setResults]=useState([]);

    // Usando promesas, pero pude volverse algo ilegible.
    /*useEffect(()=>{
        const getData = async () => {
            fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => response.json())
            .then(data => {
                setUsersData(data);
            });
        };
        getData().catch(null);
    },[]);*/

    // Otra forma usando await en lugar de las promesas. 
    /*useEffect(()=>{
        const getData = async () => {
            const response = await fetch("https://jsonplaceholder.typicode.com/users");
            const data = await response.json();
            setUsersData(data);
        };
        getData().catch(null);
    },[]);*/

    //Otra forma usadno axios
    useEffect(()=>{
        const getData = async () => {
            try {
                const data = await axios.get("https://jsonplaceholder.typicode.com/users");
                setUsersData(data.data);
            }catch (err){
                console.error(err);
            }
            
        };
        getData().catch(null);
    },[]);

    const handleCloseSearch =() => {
        setIsAtTop(false);
        setResults([]);
    };

    const handleSearchClick = (searchText) =>{
        setIsAtTop(true);
        const searchTextMinus = searchText.toLowerCase();
        if(data?.length){
            const fileterData = data.filter((value)=>(
                value.name.toLowerCase().includes(searchTextMinus) || 
                value.phone.toLowerCase().includes(searchTextMinus) || 
                value.email.toLowerCase().includes(searchTextMinus) || 
                value.username.toLowerCase().includes(searchTextMinus)
                )
            );
            setResults(fileterData);

        }
    };
    
    console.log(results);

    return(
        <div className={`search ${isAtTop ? "search--top": "search--center"}`}>
            <SearchBox onSearch={handleSearchClick} onClose={handleCloseSearch} isSearching={isAtTop}/>
            <SearchResults results={results} isSearching={isAtTop}/>
        </div>
    );
}